<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home','HomeController@index')->name('home');
Route::get('/eliminar/alumno/{id}','PerfilAlumnoController@destroy')->name('eliminar');
Route::get('/agregar','PerfilAlumnoController@index')->name('agregar');
Route::post('/guardar','PerfilAlumnoController@store')->name('guardar');
Route::get('/editar/{id}','PerfilAlumnoController@show')->name('editar');
Route::put('/actualizar/{id}','PerfilAlumnoController@update')->name('actualizar');
