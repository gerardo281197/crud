@extends('layouts.app')

@section('content')
<a href="/agregar" type="button" class="btn btn-primary">Agregar alumno</a>
<hr>
<div class="row">
    @foreach($alumnos as $alumno)
    <div class="col-md-3">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="storage/{{$alumno->foto}}" alt="Card image cap" style="width: 285px; height: 200px">
          <ul class="list-group list-group-flush">
            <li class="list-group-item">N° Control: {{ $alumno->no_control }}</li>
            <li class="list-group-item">Nombre: {{ $alumno->nombre }}</li>
            <li class="list-group-item">Hora: {{ $alumno->hora}}</li>
            <li class="list-group-item">Sexo: {{ $alumno->sexo }}</li>
            <li class="list-group-item">Carrera: {{ $alumno->carrera }}</li>
            <li class="list-group-item">Domicilio: {{ $alumno->domicilio }}</li>
          </ul>
          <div class="card-body">
            <a href="/editar/{{$alumno->id}}" type="button" class="btn btn-warning">Editar</a>
            <a href="/eliminar/alumno/{{$alumno->id}}" type="button" class="btn btn-danger">Eliminar</a>
          </div>
        </div>
    </div>
    @endforeach
</div>

@endsection
