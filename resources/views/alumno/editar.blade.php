@extends('layouts.app')

@section('content')
@foreach($alumnos as $alumno)
 <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('actualizar',$alumno->id) }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Editar Alumno') }}</h4>
                <p class="card-category">{{ __('Información') }}</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Número de control') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="no_control" type="number" value="{{ $alumno->no_control }}" aria="true"  />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="nombre" type="text" value="{{ $alumno->nombre }}" />
                    </div>
                  </div>
                </div>
               <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Hora') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="hora" type="time" value="{{ $alumno->hora }}" />
                    </div>
                  </div>
                </div>
               <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Sexo') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="1" name="sexo" class="custom-control-input" value="Hombre">
                        <label class="custom-control-label" for="1">Hombre</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="2" name="sexo" class="custom-control-input" value="Mujer">
                        <label class="custom-control-label" for="2">Mujer</label>
                      </div>
                    </div>
                  </div>
                </div>               
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Carrera') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="carrera" type="text" value="{{ $alumno->carrera }}" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Domicilio') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="domicilio" type="text" value="{{ $alumno->domicilio }}" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Foto') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="foto" type="file"  accept="image/png, image/jpeg"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
@endforeach
@endsection