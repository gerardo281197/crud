@extends('layouts.app')

@section('content')
 <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('guardar') }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Nuévo Alumno') }}</h4>
                <p class="card-category">{{ __('Información') }}</p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Número de control') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="no_control" type="number" placeholder="{{ __('Número de control') }}" required aria-required="true"  />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="nombre" type="text" placeholder="{{ __('Nombre') }}" required />
                    </div>
                  </div>
                </div>
               <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Hora') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="hora" type="time" placeholder="{{ __('Hora') }}" required />
                    </div>
                  </div>
                </div>
               <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Sexo') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="1" name="sexo" class="custom-control-input" checked="" value="Hombre">
                        <label class="custom-control-label" for="1">Hombre</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="2" name="sexo" class="custom-control-input" value="Mujer">
                        <label class="custom-control-label" for="2">Mujer</label>
                      </div>
                    </div>
                  </div>
                </div>               
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Carrera') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="carrera" type="text" placeholder="{{ __('Carrera') }}" required />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Domicilio') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="domicilio" type="text" placeholder="{{ __('Domicilio') }}" required />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Foto') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="foto" type="file"  accept="image/png, image/jpeg" title="Este archivo es requerido" required />
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
@endsection