<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PerfilAlumno;

class PerfilAlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('alumno.crear');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alumno = new PerfilAlumno;
        $alumno->no_control = $request->no_control;
        $alumno->nombre = $request->nombre;
        $alumno->hora = $request->hora;
        $alumno->sexo = $request->sexo;
        $alumno->carrera = $request->carrera;
        $alumno->domicilio = $request->domicilio;
        $path = $request->file('foto')->storeAs('public',$request->file('foto')->getClientOriginalName());
        $alumno->foto = $request->file('foto')->getClientOriginalName();
        $alumno->save();
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alumnos=PerfilAlumno::where('id',$id)->get();
        return view('alumno.editar', compact('alumnos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        dd($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ncontrol=$request->no_control;
        $nmbre=$request->nombre;
        $dir=$request->direccion;
        $sex=$request->sexo;
        $hor=$request->hora;
        $sex=$request->sexo;
        $foto=$request->file('foto');

        $alumno=PerfilAlumno::findOrFail($id);
        if( $ncontrol != null ){ $alumno->no_control = $ncontrol; }
        if( $nmbre != null ){ $alumno->nombre = $nmbre; }
        if( $dir != null ){ $alumno->direccion = $dir; }
        if( $sex != null ){ $alumno->sexo = $sex; }
        if( $hor != null ){ $alumno->hora = $hor; }
        if( $foto != null ){ 
            $alumno->foto = $request->file('foto')->getClientOriginalName(); 
            $request->file('foto')->storeAs('public',$request->file('foto')->getClientOriginalName()); }
        $alumno->update();

        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eliminar=PerfilAlumno::find($id)->delete();
        return back();
    }
}
