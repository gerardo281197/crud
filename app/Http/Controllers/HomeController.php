<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PerfilAlumno;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $siexiste=PerfilAlumno::count();
       if ($siexiste == 0) {
             return view('home');
        }else{
            $alumnos=PerfilAlumno::orderBy('id','DESC')->get();
            return view('perfiles',compact('alumnos'));
        }
        //return($siexiste);
       
    }
}
